#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>
#include <cmath>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "face_detection.h"
#include "face_alignment.h"
#include "face_identification.h"
using namespace std;

seeta::FaceDetection *g_faceDetector = NULL;
seeta::FaceIdentification *g_face_recognizer = NULL;

int main(int argc, char** argv) {
  if (argc != 2) {
      cout << "Usage: bin videoIndex"<< endl;
      return -1;
  }

  //const char* img_path = argv[1];
  cv::VideoCapture capture(0);
  capture.set(CV_CAP_PROP_FRAME_WIDTH,640 );
  capture.set(CV_CAP_PROP_FRAME_HEIGHT,480);

    g_faceDetector = new seeta::FaceDetection("");
    g_faceDetector->SetMinFaceSize(80);
    g_faceDetector->SetScoreThresh(2.f);
    g_faceDetector->SetImagePyramidScaleFactor(0.8f);
    g_faceDetector->SetWindowStep(4, 4);
    
  string MODEL_DIR = "/Users/liyang/Documents/Git/facial/sourcecode/modules/faceAlignment_seeta/model/";
  seeta::FaceAlignment point_detector((MODEL_DIR + "seeta_fa_v1.1.bin").c_str());

  std::cout<<"start load data"<<std::endl;
  MODEL_DIR = "/Users/liyang/Documents/Git/facial/sourcecode/modules/facerecognition_seeta/model/";
  //seeta::FaceIdentification face_recognizer((MODEL_DIR + "seeta_fr_v1.0.bin").c_str());
  g_face_recognizer = new seeta::FaceIdentification();//((MODEL_DIR + "seeta_fr_v1.0.bin").c_str());

  std::cout<<"find load data"<<std::endl;
  while(1){
    cv::Mat img;// = cv::imread(img_path, cv::IMREAD_UNCHANGED);
    cv::Mat img_gray;
    capture>>img;
    if(img.empty()) break;
    if (img.channels() != 1)
      cv::cvtColor(img, img_gray, cv::COLOR_BGR2GRAY);
    else
      img_gray = img;

    seeta::ImageData img_data;
    img_data.data = img_gray.data;
    img_data.width = img_gray.cols;
    img_data.height = img_gray.rows;
    img_data.num_channels = 1;
    
    std::vector<seeta::FaceInfo> faces = g_faceDetector->Detect(img_data);
   // std::cout<<faces.size()<<" faces detected!"<<std::endl;

   
    int32_t num_face = static_cast<int32_t>(faces.size());
    if(num_face == 0){
        cv::imshow("disp",img);
        cv::waitKey(10);
        continue;
    }
    cv::Rect face_rect;
    for (int32_t i = 0; i < num_face; i++) {
        face_rect.x = faces[i].bbox.x;
        face_rect.y = faces[i].bbox.y;
        face_rect.width = faces[i].bbox.width;
        face_rect.height = faces[i].bbox.height;
       // cv::rectangle(img, face_rect, CV_RGB(0, 0, 255), 4, 8, 0);
        seeta::FacialLandmark points[5];
        point_detector.PointDetectLandmarks(img_data, faces[i], points);

        // Visualize the results
        int  pts_num = 5; 
        cv::rectangle(img, cv::Point(faces[i].bbox.x, faces[i].bbox.y), cv::Point(faces[i].bbox.x + faces[i].bbox.width - 1, faces[i].bbox.y + faces[i].bbox.height - 1), CV_RGB(255, 0, 0));
        for (int i = 0; i<pts_num; i++)
        {
            cv::circle(img, cv::Point(points[i].x, points[i].y), 2, CV_RGB(0, 255, 0), CV_FILLED);
        }

        cv::Mat dst_img(g_face_recognizer->crop_height(),
        g_face_recognizer->crop_width(),
        CV_8UC(g_face_recognizer->crop_channels()));
        seeta::ImageData dst_img_data(dst_img.cols, dst_img.rows, dst_img.channels());
        dst_img_data.data = dst_img.data;
        /* Crop Face */
       // std::cout<<img_data.data<<std::endl;
        g_face_recognizer->CropFace(img_data, points, dst_img_data);
        cv::imshow("cropimage",dst_img);
    }

    
    cv::imshow("disp",img);
    cv::waitKey(10);
  }
}
