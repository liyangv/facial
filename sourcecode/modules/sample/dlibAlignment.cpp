#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>
#include <cmath>
#include <sys/time.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "face_detection.h"
#include "face_alignment.h"

#include <dlib/image_processing.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/gui_widgets.h>
#include <dlib/opencv/cv_image.h>
using namespace dlib;


using namespace std;

seeta::FaceDetection *g_faceDetector = NULL;

shape_predictor sp;

typedef enum _FileType_t
{
	FILE_TYPE_IMAGE = 0,
	FILE_TYPE_VIDEO = 1,
	FILE_TYPE_IMAGE_LIST = 2,
	FILE_TYPE_CAMERA = 3,
	FILE_TYPE_IMAGE_DIR = 4,
	FILE_TYPE_AMUVC = 5,
	FILE_TYPE_NONE,
}FileType_t;


FileType_t fileType;
int main(int argc, char** argv) {
  if (argc != 3) {
      cout << "Usage: bin videoIndex shapedata"<< endl;
      return -1;
  }

  deserialize(argv[2]) >> sp;

  std::string fileName =  argv[1];
  std::string postfix;
	size_t found = fileName.find_last_of('.');
	if(found != string::npos)
		  postfix = fileName.substr(found);
	if(postfix.length() >= 4)
	{
			if(!strncasecmp(postfix.c_str(), ".jpg", 4) || !strncasecmp(postfix.c_str(), ".png", 4) || !strncasecmp(postfix.c_str(), ".bmp", 4))
			{
				fileType = FILE_TYPE_IMAGE;
			}else
			{
				cout<<"file type error:"<<fileName<<endl;
			}
	}
	else if(fileName.length() == 1 && isdigit(*fileName.c_str()))
	{
			cout << "camera index:" << fileName << endl;
			fileType = FILE_TYPE_CAMERA;
	}else{
      cout<<"file type error:"<<fileName<<endl;
  }

  cv::namedWindow("disp", CV_WINDOW_NORMAL);
  cvSetWindowProperty( "disp", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN );
  cvMoveWindow("disp", 0, 0);
  //cv::startWindowThread();

  g_faceDetector = new seeta::FaceDetection("");
  g_faceDetector->SetMinFaceSize(120);
  g_faceDetector->SetScoreThresh(2.8f);
  g_faceDetector->SetImagePyramidScaleFactor(0.8f);
  g_faceDetector->SetWindowStep(4, 4);
    
  if(fileType == FILE_TYPE_CAMERA)
  {
      cv::VideoCapture capture(atoi(fileName.c_str()));
      capture.set(CV_CAP_PROP_FRAME_WIDTH,1920);
      capture.set(CV_CAP_PROP_FRAME_HEIGHT,1080);

      while(1){
        cv::Mat img;// = cv::imread(img_path, cv::IMREAD_UNCHANGED);
        cv::Mat img_gray;
        capture>>img;
        if(img.empty()) break;
        if (img.channels() != 1)
          cv::cvtColor(img, img_gray, cv::COLOR_BGR2GRAY);
        else
          img_gray = img;

        seeta::ImageData img_data;
        img_data.data = img_gray.data;
        img_data.width = img_gray.cols;
        img_data.height = img_gray.rows;
        img_data.num_channels = 1;
        
        timeval time0,time1,time2;
        gettimeofday(&time0, NULL);

        std::vector<seeta::FaceInfo> faces = g_faceDetector->Detect(img_data);
        gettimeofday(&time1, NULL);

      
        int32_t num_face = static_cast<int32_t>(faces.size());
        if(num_face == 0){
            cv::imshow("disp",img);
            cv::waitKey(10);
            continue;
        }
        //std::vector<rectangle> dets;
        //array2d<rgb_pixel> img;
        cv::Rect face_rect;
        for (int32_t i = 0; i < num_face; i++) {
            face_rect.x = faces[i].bbox.x;
            face_rect.y = faces[i].bbox.y;
            face_rect.width = faces[i].bbox.width;
            face_rect.height = faces[i].bbox.height;
          //  dets.push_back(face_rect);

            //std::vector<full_object_detection> shapes;
            dlib::rectangle rec(face_rect.x, face_rect.y, face_rect.x+face_rect.width, face_rect.y+face_rect.height);
            dlib::full_object_detection shape = sp(dlib::cv_image<uchar>(img_gray), rec);
            std::vector<image_window::overlay_line> lines = render_face_detections(shape);
            for(int j = 0;j < lines.size();j++){
              image_window::overlay_line line = lines[j];
              //cv::line(img,cv::Point(line.p1.x(),line.p1.y()),cv::Point(line.p2.x(),line.p2.y()),CV_RGB(0, 255, 0),2);
              cv::circle(img, cv::Point(line.p1.x(),line.p1.y()), 3, CV_RGB(0, 255, 0), CV_FILLED);
             cv::circle(img, cv::Point(line.p2.x(),line.p2.y()), 3, CV_RGB(0, 255, 0), CV_FILLED);
            }
        }
        gettimeofday(&time2, NULL);
        std::cout << "face Detect Time: " << (time1.tv_sec-time0.tv_sec)*1000.0 + (time1.tv_usec-time0.tv_usec)/1000.0 << "(ms)" << std::endl;
        std::cout << "landmark Detect Time: " << (time2.tv_sec-time1.tv_sec)*1000.0 + (time2.tv_usec-time1.tv_usec)/1000.0 << "(ms)" << std::endl;
        cv::imshow("disp",img);
        cv::waitKey(10);
      }
  }else{
        cv::Mat img;// = cv::imread(img_path, cv::IMREAD_UNCHANGED);
        cv::Mat img_gray;
        img = cv::imread(fileName);
        if(img.empty()) return -1;
        if (img.channels() != 1)
          cv::cvtColor(img, img_gray, cv::COLOR_BGR2GRAY);
        else
          img_gray = img;

        seeta::ImageData img_data;
        img_data.data = img_gray.data;
        img_data.width = img_gray.cols;
        img_data.height = img_gray.rows;
        img_data.num_channels = 1;
        
        std::vector<seeta::FaceInfo> faces = g_faceDetector->Detect(img_data);
      // std::cout<<faces.size()<<" faces detected!"<<std::endl;

      
        int32_t num_face = static_cast<int32_t>(faces.size());
        if(num_face == 0){
            cv::imshow("disp",img);
            cv::waitKey(0);
            return -1;
        }
         cv::Rect face_rect;
        for (int32_t i = 0; i < num_face; i++) {
            face_rect.x = faces[i].bbox.x;
            face_rect.y = faces[i].bbox.y;
            face_rect.width = faces[i].bbox.width;
            face_rect.height = faces[i].bbox.height;
          //  dets.push_back(face_rect);

            //std::vector<full_object_detection> shapes;
            dlib::rectangle rec(face_rect.x, face_rect.y, face_rect.x+face_rect.width, face_rect.y+face_rect.height);
            dlib::full_object_detection shape = sp(dlib::cv_image<uchar>(img_gray), rec);
            std::vector<image_window::overlay_line> lines = render_face_detections(shape);
            for(int j = 0;j < lines.size();j++){
              image_window::overlay_line line = lines[j];
             // cv::line(img,cv::Point(line.p1.x(),line.p1.y()),cv::Point(line.p2.x(),line.p2.y()),CV_RGB(0, 255, 0),2);
             cv::circle(img, cv::Point(line.p1.x(),line.p1.y()), 3, CV_RGB(0, 255, 0), CV_FILLED);
             cv::circle(img, cv::Point(line.p2.x(),line.p2.y()), 3, CV_RGB(0, 255, 0), CV_FILLED);
            }
        }
       /* cv::Rect face_rect;
        for (int32_t i = 0; i < num_face; i++) {
            face_rect.x = faces[i].bbox.x;
            face_rect.y = faces[i].bbox.y;
            face_rect.width = faces[i].bbox.width;
            face_rect.height = faces[i].bbox.height;
          // cv::rectangle(img, face_rect, CV_RGB(0, 0, 255), 4, 8, 0);
            seeta::FacialLandmark points[5];
            point_detector.PointDetectLandmarks(img_data, faces[i], points);

            // Visualize the results
            int  pts_num = 5; 
            cv::rectangle(img, cv::Point(faces[i].bbox.x, faces[i].bbox.y), cv::Point(faces[i].bbox.x + faces[i].bbox.width - 1, faces[i].bbox.y + faces[i].bbox.height - 1), CV_RGB(255, 0, 0),5);
            for (int i = 0; i<pts_num; i++)
            {
                cv::circle(img, cv::Point(points[i].x, points[i].y), 6, CV_RGB(0, 255, 0), CV_FILLED);
            }
        
        }*/
        cv::imwrite("save.png",img);
        cv::imshow("disp",img);
        cv::waitKey(0);
  }
  
}
