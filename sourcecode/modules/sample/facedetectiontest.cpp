#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>
#include <cmath>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
//#include "face_detectionImp.h"
#include "face_detection.h"

using namespace std;

seeta::FaceDetection *g_faceDetector = NULL;


int main(int argc, char** argv) {
  if (argc != 2) {
      cout << "Usage: bin videoIndex"<< endl;
      return -1;
  }

  //const char* img_path = argv[1];
  cv::VideoCapture capture(0);
  capture.set(CV_CAP_PROP_FRAME_WIDTH,1920 );
  capture.set(CV_CAP_PROP_FRAME_HEIGHT,1080);

    g_faceDetector = new seeta::FaceDetection("");
    g_faceDetector->SetMinFaceSize(80);
    g_faceDetector->SetScoreThresh(2.f);
    g_faceDetector->SetImagePyramidScaleFactor(0.8f);
    g_faceDetector->SetWindowStep(4, 4);
    

  while(1){
    cv::Mat img;// = cv::imread(img_path, cv::IMREAD_UNCHANGED);
    cv::Mat img_gray;
    capture>>img;
    if(img.empty()) break;
    if (img.channels() != 1)
      cv::cvtColor(img, img_gray, cv::COLOR_BGR2GRAY);
    else
      img_gray = img;

    seeta::ImageData img_data;
    img_data.data = img_gray.data;
    img_data.width = img_gray.cols;
    img_data.height = img_gray.rows;
    img_data.num_channels = 1;
    
    std::vector<seeta::FaceInfo> faces = g_faceDetector->Detect(img_data);
   // std::cout<<faces.size()<<" faces detected!"<<std::endl;

    int32_t num_face = static_cast<int32_t>(faces.size());
    cv::Rect face_rect;
    for (int32_t i = 0; i < num_face; i++) {
        face_rect.x = faces[i].bbox.x;
        face_rect.y = faces[i].bbox.y;
        face_rect.width = faces[i].bbox.width;
        face_rect.height = faces[i].bbox.height;
        cv::rectangle(img, face_rect, CV_RGB(0, 0, 255), 4, 8, 0);
    }

    cv::imshow("disp",img);
    cv::waitKey(10);
  }
}
